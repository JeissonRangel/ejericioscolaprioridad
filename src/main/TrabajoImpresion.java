/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author Usuario
 */
public class TrabajoImpresion implements Comparable<TrabajoImpresion> {
    
    int cantHojas;
    String nombre;
    
    public TrabajoImpresion(int hojas, String nom){
        cantHojas = hojas;
        nombre = nom;
    }

    public int getCantHojas() {
        return cantHojas;
    }

    public void setCantHojas(int cantHojas) {
        this.cantHojas = cantHojas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    

    public boolean equals(TrabajoImpresion a){
    return this.getCantHojas()==a.getCantHojas();
    }
    
    @Override
    public int compareTo(TrabajoImpresion a) {
    
        if(this.equals(a))
            return 0;
        else if(this.getCantHojas()>a.getCantHojas())
            return 1;
        else
            return -1;
    
    
    }

    @Override
    public String toString() {
        return "TrabajoImpresion ::" + "cantHojas = " + cantHojas + " nombre = " + nombre + "\n";
    }
    
    
    
}
