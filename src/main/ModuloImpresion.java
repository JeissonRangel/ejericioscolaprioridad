/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.PriorityQueue;

/**
 *
 * @author Usuario
 */
public class ModuloImpresion {
    
    PriorityQueue<TrabajoImpresion> impresora;

    public ModuloImpresion() {
    
        impresora = new PriorityQueue();
        
    }
    
    
    
    public String añadirImpresion(int a, String nom){
    
        
        impresora.add(new TrabajoImpresion(a,nom));
        
        
    return "cantidad de hojas de este trabajo "+a;
    }
    

    public String imprimirTrabajo(){
    String cad="";
    
        if (impresora.isEmpty()) {
            return "algo malo paso";
        }
        
        cad=impresora.peek().getNombre();
        impresora.remove();
        
    return "se imprimio el documento "+cad;    
    }
    
    public String mostrarTareas(){
        String cad="";
        
        for (TrabajoImpresion a : impresora) {
            cad+=a.toString();
        }
        
    return cad;
    }
    
    public String archivoImprimiendo(){
    return impresora.peek().getNombre();
    }
    
    public String testRemove(){
    String cad ="";
        while(!impresora.isEmpty()){
            cad+=impresora.remove().toString();
        }
    return cad;
    }
    
    public static void main(String[] args) {
        
        ModuloImpresion imp = new ModuloImpresion();
        
        imp.añadirImpresion(2, "PREVIO POO2 2");
        imp.añadirImpresion(3, "PREVIO POO2 3");
        imp.añadirImpresion(9, "PREVIO POO2 9");
        imp.añadirImpresion(5, "PREVIO POO2 5");
        System.out.println(imp.mostrarTareas());
        
        System.out.println(imp.imprimirTrabajo());
        System.out.println(imp.imprimirTrabajo());
        System.out.println(imp.imprimirTrabajo());
        System.out.println(imp.imprimirTrabajo());
        
    }
    
}
